<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <script src="assets/js/fontawesome.js"></script>
    <script src="assets/js/lance.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <link rel="stylesheet" href="assets/css/lance.css">
    <title>Display de lance</title>
</head>
<body style="background-color: aliceblue">

    <input type="text" name="lance_atual" id="lance_atual" class="inputPrincipal" value="0" readonly>

    <div class="d-flex justify-content-center mt-5">
        <!--    Aumentar lance -->
        <div class="btn-toolbar" role="toolbar" aria-label="Aumentar lance">
            <div class="btn-group" role="group" aria-label="Aumentar">
                <button type="button" class="btn btn-lg btn-primary" onclick="alterarValorFixoLance(5);">+05</button>
                <button type="button" class="btn btn-lg btn-primary" onclick="alterarValorFixoLance(10);">+10</button>
                <button type="button" class="btn btn-lg btn-primary" onclick="alterarValorFixoLance(20);">+20</button>
                <button type="button" class="btn btn-lg btn-primary" onclick="alterarValorFixoLance(50);">+50</button>
                <button type="button" class="btn btn-lg btn-primary" onclick="alterarValorFixoLance(100);">+100</button>

            </div>
        </div>
    </div>

    <div class="d-flex justify-content-center mt-3">
        <!--    Grupo de botões para diminuir lance -->
        <div class="btn-toolbar" role="toolbar" aria-label="Diminuir lance">
            <div class="btn-group" role="group" aria-label="Diminuir">
                <button type="button" class="btn btn-lg btn-secondary" onclick="alterarValorFixoLance(-5);">&nbsp;-05</button>
                <button type="button" class="btn btn-lg btn-secondary" onclick="alterarValorFixoLance(-10);">&nbsp;-10</button>
                <button type="button" class="btn btn-lg btn-secondary" onclick="alterarValorFixoLance(-20);">&nbsp;-20</button>
                <button type="button" class="btn btn-lg btn-secondary" onclick="alterarValorFixoLance(-50);">&nbsp;-50</button>
                <button type="button" class="btn btn-lg btn-secondary" onclick="alterarValorFixoLance(-100);">&nbsp;-100</button>
            </div>
        </div>
    </div>


    <!-- FLEXBOX PARA ALINHAMENTO DOS INPUTS E BOTÃO-->
    <div class="d-flex justify-content-center mt-3">
        <div class="p-2 bd-highlight">
            <label for="lanceInicial">Lance Inicial</label>
            <input type="number" name="lanceInicial" id="lanceInical" onkeyup="definirLanceInicial();"
                   onclick="this.select()" value="0" min="0" class="form-control form-control-lg">
        </div>
        <div class="p-2 bd-highlight">
            <label for="intervaloAumento">Valor (+ ou -)</label>
            <input type="number" name="intervaloAumento" id="intervaloAumento" onclick="this.select()" value="10"
                   class="form-control form-control-lg">

        </div>
        <div class="p-2 bd-highlight">
            <label for="aumentar">&nbsp;</label><br/>
            <button
                    class="btn btn-lg btn-warning btn-block"
                    id="aumentar"
                    onclick="alterarValorLance();"
                    data-toggle="tooltip"
                    data-placement="right"
                    title="Alterar valor">
                    <i class="fas fa-expand-alt"></i>
                    Alterar
            </button>

        </div>
    </div>
    <div class="d-flex justify-content-center mt-3">


        <div class="p-2 bd-highlight">
            <label for="txtParcelas">Parcelas</label>
            <input
                    type="text"
                    class="form-control form-control-lg"
                    name="txtParcelas"
                    id="txtParcelas"
                    onclick="this.select()"
                    value=""
            >
        </div>
        <div class="p-2 bd-highlight">
            <label for="salvarParcelas">&nbsp;</label><br/>
            <button
                    class="btn btn-lg btn-warning btn-block"
                    id="salvarParcelas"
                    onclick="salvarParcelas(document.getElementById('txtParcelas').value);"
                    data-toggle="tooltip"
                    data-placement="right"
                    title="Salvar parcelas">
                <i class="fa-solid fa-arrow-rotate-right"></i> Atualizar
            </button>
        </div>

    </div>
    <div class="d-flex justify-content-center mt-3">
        <div id="lblParcelaSalva" class="font-weight-bold">

        </div>
    </div>
</body>
</html>