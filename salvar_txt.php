<?php

if (isset($_GET['conteudo_txt']) && isset($_GET['lanceOuParcelas'])) {
    // Sanitize and validate user input
    $conteudo_txt = isset($_GET['conteudo_txt']) ? trim($_GET['conteudo_txt']) : '';
    $lanceOuParcelas = isset($_GET['lanceOuParcelas']) ? trim($_GET['lanceOuParcelas']) : '';

    $lanceFileName = 'lance.txt';
    $parcelasFileName = 'parcelas.txt';

    $filename = ($lanceOuParcelas == 'lance') ? $lanceFileName : $parcelasFileName;

    $filePath = __DIR__ . '/' . $filename;

    if (file_put_contents($filePath, $conteudo_txt) !== false) {
        echo "Arquivo salvo com sucesso.";
    } else {
        echo "Erro ao salvar o arquivo. Confira permissões de escrita.";
    }
} else {
    echo "Erro ao salvar arquivo.";
    die;
}



//    if (isset($_GET['conteudo_txt']) && isset($_GET['lanceOuParcelas']))
//    {
//        $conteudo_txt = trim($_GET['conteudo_txt']);
//        $lanceOuParcelas = trim($_GET['lanceOuParcelas']);
//
//        if ($lanceOuParcelas == 'lance') {
//            $arquivo = fopen('lance.txt', 'w');
//        }
//        else {
//            $arquivo = fopen('parcelas.txt', 'w');
//        }
//
//        if (fwrite($arquivo, $conteudo_txt)) {
//            fclose($arquivo);
//        }
//    } else {
//        echo "Erro ao salvar arquivo.";die;
//    }