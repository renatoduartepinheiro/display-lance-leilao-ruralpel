function preparaStr(lance)
{
    lance = lance.replace(".", "");
    lance = lance.replace(",", ".");
    return  parseInt(lance);
}

function alterarValorLance()
{
    let lanceTela;
    if (document.getElementById("lanceInical").value !== '' && document.getElementById("intervaloAumento").value !== '') {
        lanceTela = String(document.getElementById("lance_atual").value);
        const valor = parseInt(String(document.getElementById("intervaloAumento").value));
        document.getElementById("lance_atual").value = parseInt(preparaStr(lanceTela) + valor).toLocaleString('pt-BR', {minimumFractionDigits: 2});
        gerarTxt('lance', document.getElementById("lance_atual").value);

    }
}

/**
 * Define o lance inicial
 *
 */
function definirLanceInicial()
{
    if (document.getElementById("lanceInical").value !== '') {
        document.getElementById("lance_atual").value = preparaStr(String(document.getElementById("lanceInical").value)).toLocaleString('pt-BR', {minimumFractionDigits: 2});
    }
    else {
        document.getElementById("lance_atual").value = '0,00';
    }

    gerarTxt('lance', document.getElementById("lance_atual").value);
}

/**
 * Altera o valor do lance atual
 * @param valor
 */
function alterarValorFixoLance(valor)
{
    let lanceTela = String(document.getElementById("lance_atual").value);
    document.getElementById("lance_atual").value = parseInt(preparaStr(lanceTela) + valor).toLocaleString('pt-BR', { minimumFractionDigits: 2});
    gerarTxt('lance', document.getElementById("lance_atual").value);
}

/**
 * Grava um arquivo de texto com o lance atual ou as parcelas
 * @param lanceOuParcelas
 * @param valorParaGravar
 */
function gerarTxt(lanceOuParcelas, valorParaGravar)
{
    let ajax = new XMLHttpRequest();
    let encodedLanceOuParcelas = encodeURIComponent(lanceOuParcelas);
    let encodedValorParaGravar = encodeURIComponent(valorParaGravar);

    ajax.open("GET", "salvar_txt.php?lanceOuParcelas=" + encodedLanceOuParcelas + "&conteudo_txt=" + encodedValorParaGravar, true);

    ajax.onload = function () {
        if (ajax.status >= 200 && ajax.status < 300) {
            // Request was successful
            console.log(ajax.responseText);
        } else {
            // Request failed
            console.error("Error in the request: " + ajax.statusText);
        }
    };

    ajax.onerror = function () {
        // There was a network error
        console.error("Network error occurred");
    };

    ajax.send();

}

/**
 * Salva as parcelas no arquivo txt
 * @param textoParcelas
 */
function salvarParcelas(textoParcelas)
{
    textoParcelas = textoParcelas.trim();
    gerarTxt('parcelas', textoParcelas);
    document.getElementById("lblParcelaSalva").innerHTML = 'Atual: ' + textoParcelas;
}